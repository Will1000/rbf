export class Neuron {
  nEntradas: number;
  nSalidas: number;
  nPatrones: number;
  nCentrosRadiales: number;
  datos: number[][];
  errorOptimo: number;
  fActivacion: string
  minEntrada: number;
  maxEntrada: number;
}
