import { NodeResponse } from "./NodeResponse";
import { RadialCenterResponse } from "./RadialCenterResponse";

export class RbfResponse {
  num_inputs: number;
  radial_centers: RadialCenterResponse[];
  output_node: NodeResponse;
  errors: number[];
}
