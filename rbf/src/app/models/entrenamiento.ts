export class Entrenamiento {

  rAprendizaje: number;
  errorOptimo: number;
  fActivacion: string
  distancia: number[][];
  errorLineal: number[];
  errorPatron: number[];
  errorRMS: number;
}
