import { OnInit } from '@angular/core';
import { Entrenamiento } from 'src/app/models/entrenamiento';
import { Neuron } from 'src/app/models/neuron';
import {Component,ViewChild} from '@angular/core';
import {MatTable} from '@angular/material/table';
import { Router } from '@angular/router';
import { GraficResultService } from 'src/app/services/grafic-result.service';
import { EntrenamientoService } from 'src/app/services/entrenamiento.service';

export interface PeriodicElement {
  index: number;
  entradas: string;
  sDeseada: string;
  sObtenida: string;
}


@Component({
  selector: 'app-simulacion',
  templateUrl: './simulacion.component.html',
  styleUrls: ['./simulacion.component.css']
})
export class SimulacionComponent implements OnInit {

  data: PeriodicElement[] = [];

  displayedColumns: string[] = ['index','entradas', 'sDeseada', 'sObtenida'];
  dataSource = [...this.data];

  @ViewChild(MatTable, {static: false}) table: MatTable<PeriodicElement>;


  fileContent: string;
  neurona: Neuron;
  entrenamiento: Entrenamiento;
  matrizPesos: number[][] = [];
  umbrales: number[] = [];
  sObtenidas: number[] = [];

  constructor(
    private router: Router,
    private graficResultService: GraficResultService,
    private entrenamientoService: EntrenamientoService
    ) { }

  ngOnInit(){}


  goEntrenar() {
    this.router.navigate(['/']);
  }

  addFila(fila: any) {
    this.dataSource.push(fila);
    this.table.renderRows();
  }

  public onChange(fileList: FileList): void {
    this.neurona = new Neuron();
    this.entrenamiento = new Entrenamiento();

    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    fileReader.readAsText(file);

    let self = this;
    fileReader.onload = e => {

      self.fileContent = fileReader.result as string;
      var rows = self.fileContent.split('\r\n');
      this.neurona.nSalidas = rows[0].split(';').length -1;
      this.neurona.nEntradas = rows[0].split(',').length;


      this.neurona.datos = [];
      for (var i = 1; i < rows.length; i++) {
        if (rows[i].length != 0) {
          this.neurona.datos.push(rows[i].split(',').map(Number));
        }
      }
      this.neurona.nPatrones = this.neurona.datos.length;
      console.log("Entradas: "+this.neurona.nEntradas);
      console.log("Salidas: "+this.neurona.nSalidas);
      console.log("Patrones: "+this.neurona.nPatrones)
      console.log(this.neurona.datos);

    };
  }

  iniciarGrafica() {
    this.dataGrafic[0].series.push({name:0, value: 0});
    this.dataGrafic[1].series.push({name:0, value: 0});
  }

  obetenerEntradas() {

    let row: number[];

    for (let i = 0; i < 1; i++) {
      row =[];
      for (let j = 0; j < this.neurona.nEntradas; j++) {
        row.push(this.neurona.datos[i][j])
      }
    }

    return row;
  }

  obetenerSalida() {
    return this.neurona.datos[0][this.neurona.nEntradas];
  }



  iniciarSimulacion() {

    const entradas = this.obetenerEntradas();
    const data = {inputs: entradas};
    this.entrenamientoService.eval(data).subscribe(result => {
      console.log(result);
      this.addFila({index: 1,entradas: this.obetenerEntradas().toString(),
        sDeseada: this.obetenerSalida().toString(), sObtenida: result.toString()})

        this.dataGrafic[0].series.push({name:1, value: result});
        this.dataGrafic[1].series.push({name:1, value: this.obetenerSalida()});

        this.graficResultService.addGraficSimu(this.dataGrafic);
        console.log(this.graficResultService.resultGraficSimu);
    })




  }





  main() {

    this.iniciarGrafica();
    this.iniciarSimulacion();


  }




    //------------------------------------------Grafica ------------------------


    view: any[] = [700, 300];

    // options
    legend: boolean = true;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'interaciones';
    yAxisLabel: string = 'YR';
    timeline: boolean = true;

    colorScheme = {
      domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
    };



    onSelect(data): void {
      console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
      console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
      console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }


    // --------------Data ------------------------

    dataGrafic = [
      {
        name: 'YR',
        series: [
        ],
      },
      {
        name: 'YD',
        series: [
        ],
      }
    ];

    get multi() {
      return this.graficResultService.resultGraficSimu;
    }



}
