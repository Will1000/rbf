import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Entrenamiento } from 'src/app/models/entrenamiento';
import { Neuron } from 'src/app/models/neuron';
import { GraficResultService } from 'src/app/services/grafic-result.service';
import { LocalstoreService } from 'src/app/services/localstore.service';
import {MatSnackBar} from '@angular/material';
import { EntrenamientoService } from 'src/app/services/entrenamiento.service';


@Component({
  selector: 'app-entrenamiento',
  templateUrl: './entrenamiento.component.html',
  styleUrls: ['./entrenamiento.component.css']
})
export class EntrenamientoComponent implements OnInit {

  constructor(
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private graficResultService: GraficResultService,
    private localStoreService: LocalstoreService,
    private router: Router,
    private entrenamientoService: EntrenamientoService
    ) {

  }

    // pruebas

  form: FormGroup;
  fileContent: string;
  neurona: Neuron;
  entrenamiento: Entrenamiento;
  centrosRadiales: number[][] = [];
  row: number[] = [];

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      fActivacion: [
        '',
        {
          validators: [Validators.required],

        }
      ],
      errorOptimo: [
        '',
        {
          validators: [Validators.required, Validators.min(0), Validators.max(1)]
        }
      ],
      nCentrosRadiales: [
        '',
        {
          validators: [Validators.required, Validators.min(0), Validators.max(100)]
        }
      ],
    });
  }

  // Validaciones

  getErrorComboFActivacion() {
    var field = this.form.get('fActivacion');

    if (field.hasError('required')) {
      return 'El campo Funcion de activacion es requerido';
    }
  }

  getErrorErrorOptimo() {
    var field = this.form.get('errorOptimo');

    if (field.hasError('required')) {
      return 'El campo Error de aproximacion optimo es requerido';
    }

    if (field.hasError('min')) {
      return 'El campo Error de aproximacion optimo  debe ser de minimo 0';
    }
    if (field.hasError('max')) {
      return 'El campo Error de aproximacion optimo  debe ser de maximo 1';
    }
    return '';
  }
  getErrorCentrosRadiales() {
    var field = this.form.get('nCentrosRadiales');

    if (field.hasError('required')) {
      return 'El campo Numero de centros radiales es requerido';
    }

    if (field.hasError('min')) {
      return 'El campo Numero de centros radiales debe ser de minimo 0';
    }
    if (field.hasError('max')) {
      return 'El campo Numero de centros radiales debe ser de maximo 100';
    }
    return '';
  }

  // Metodos get

  get multi() {
    return this.graficResultService.resultGrafic;
  }

  get isInvalidParametrosEntrada() {
    return this.form.get('nIteraciones').invalid || this.form.get('errorMaximo').invalid;
  }

  get isInvalidConfigRed() {
    return this.form.get('fActivacion').invalid ||
    this.form.get('errorOptimo').invalid;
  }

  get centrosValidos() {

    return this.centrosRadiales.length == 0;
  }

  // Cargar archivos

  public onChange(fileList: FileList): void {
    this.neurona = new Neuron();
    this.entrenamiento = new Entrenamiento();

    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    fileReader.readAsText(file);

    let self = this;
    fileReader.onload = e => {

      self.fileContent = fileReader.result as string;
      var rows = self.fileContent.split('\r\n');
      this.neurona.nSalidas = rows[0].split(';').length -1;
      this.neurona.nEntradas = rows[0].split(',').length;


      this.neurona.datos = [];
      for (var i = 1; i < rows.length; i++) {
        if (rows[i].length != 0) {
          this.neurona.datos.push(rows[i].split(',').map(Number));
        }
      }
      this.neurona.nPatrones = this.neurona.datos.length;
      // this.setMaxMin(this.neurona.datos);
      console.log('Neurona: '); console.log(this.neurona);
    };
  }

  mostarPesosxxx() {
    this.entrenamiento = this.form.value;
    console.log(this.entrenamiento);
    // console.log(this.form.get('arrayPesos').value);
    // this.ordenarPesos(this.form.get('arrayPesos').value);
    // console.log(this.centrosRadiales);
  }


  guardarNeurona(message: string, action: string) {
    this.localStoreService.set("pesos", this.centrosRadiales);
    this.localStoreService.set("fActivacion", this.entrenamiento.fActivacion);
    this._snackBar.open(message, action);
  }




 getArray(valor: number): number[] {
  let array: number[] = [];
  for (let i = 0; i < valor; i++) {
   array.push(i);
  }
  return array;
}

  iniciarGrafica() {
    this.data[0].series.push({name: 0, value: 0});
  }

  goSimulacion() {
    this.router.navigate(['/simulacion']);
  }



  obetenerEntradas() {

    let row: number[];
    let matizEntradas: number[][] = [];
    let vectorEntrada: number[] = [];

    for (let i = 0; i < this.neurona.datos.length; i++) {
      row =[];
      for (let j = 0; j < this.neurona.nEntradas; j++) {
        row.push(this.neurona.datos[i][j]);
        vectorEntrada.push(this.neurona.datos[i][j]);
      }

      matizEntradas.push(row);
    }

    this.neurona.maxEntrada = Math.max(...vectorEntrada);
    this.neurona.minEntrada = Math.min(...vectorEntrada);

    return matizEntradas;
  }



  obtenerSalidas() {
    let tamFila = this.neurona.datos[0].length;
    let vectorSalida = [];

    for (let i = 0; i < this.neurona.nPatrones; i++) {
      for (let j = this.neurona.nEntradas; j < tamFila; j++) {
        vectorSalida.push(this.neurona.datos[i][j]);
      }
    }

    return vectorSalida;
  }

  asignarCapos() {
    this.neurona.nCentrosRadiales = this.form.get('nCentrosRadiales').value;
    this.neurona.errorOptimo = this.form.get('errorOptimo').value;
    this.neurona.fActivacion = this.form.get('fActivacion').value;
  }

  initNeuron() {

    this.asignarCapos();
    console.log(this.neurona);

    const data = {num_inputs: this.neurona.nEntradas, radial_centers: this.neurona.nCentrosRadiales}
    this.entrenamientoService.init(data).subscribe(result => {
      console.log(result);
      this.obetenerEntradas();
      this.centrosRadiales =  this.entrenamientoService.inicializarCentros(this.neurona);

    });
  }





  main(){



    const entradas = this.obetenerEntradas();
    const salidas = this.obtenerSalidas();

    const data = {inputs: entradas, outputs: salidas, tolerance: this.neurona.errorOptimo, epochs:1 }

    this.entrenamientoService.fit(data).subscribe(result => {
      console.log(result);
      console.log(result.errors[0]);


      this.data[0].series.push({name:0, value: 1});
      this.data[1].series.push({name:0, value: 1});

      this.data[0].series.push({name:1, value: result.errors[0]});
      this.data[1].series.push({name:1, value: this.neurona.errorOptimo});
      this.graficResultService.addGrafic(this.data);
      console.log(this.graficResultService.resultGrafic);

    });

  }



// Grafica

  view: any[] = [700, 300];
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'interaciones';
  yAxisLabel: string = 'Error MRS';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };



  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }


  // Data Grafica

  data = [
     {
        name: 'EG',
        series: [
        ],
      },
      {
        name: 'EAO',
        series: [
        ],
      }
  ];


}
