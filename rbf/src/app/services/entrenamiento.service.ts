import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HandleHttpErrorService } from '../@base/handle-http-error.service';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Neuron } from '../models/neuron';
import { RbfResponse } from '../models/RbfResponse';

@Injectable({
  providedIn: 'root'
})
export class EntrenamientoService {

  baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient,
              private handleErrorService: HandleHttpErrorService) {
  }

  init(init: any): Observable<RbfResponse> {
    console.log(init);
    return this.http.post<RbfResponse>(this.baseUrl + 'rbf/init', init)
           .pipe(tap(_ => this.handleErrorService.log('Datos enviados')),
            catchError(this.handleErrorService.handleError<RbfResponse>('Error al inicial la neurona' , null))
    );
  }

  fit(fit: any): Observable<RbfResponse> {
    console.log(fit);
    return this.http.post<RbfResponse>(this.baseUrl + 'rbf/fit', fit)
           .pipe(tap(_ => this.handleErrorService.log('Datos enviados')),
            catchError(this.handleErrorService.handleError<RbfResponse>('Error al entrenar la neurona' , null))
    );
  }

  eval(inputs: any): Observable<number> {
    console.log(inputs);
    return this.http.patch<number>(this.baseUrl + 'rbf/eval', inputs)
           .pipe(tap(_ => this.handleErrorService.log('Datos enviados')),
            catchError(this.handleErrorService.handleError<number>('Error al simular la neurona' , null))
    );
  }










  salidaRed(salida: number): number {
    let result = (Math.pow(salida,2) * Math.log(salida));
    return parseFloat(result.toFixed(2));
  }



  inicializarCentros(neurona: Neuron): number[][] {

    let matrizCentros: number[][] = [];

    let row: number[] = [];
    for (let i = 0; i < neurona.nCentrosRadiales; i++) {
      row = [];
      for (let j = 0; j < neurona.nEntradas; j++) {
        row.push(parseFloat((this.getRandomArbitrary(neurona.minEntrada, neurona.maxEntrada)).toFixed(2)));
      }
      matrizCentros.push(row);
    }

    return matrizCentros;
  }

  getRandomArbitrary(min:number, max: number) {
    return Math.random() * (max - min) + min;
  }



}
